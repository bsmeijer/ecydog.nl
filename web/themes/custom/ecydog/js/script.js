(function ($) {
	'use strict';

	Drupal.behaviors.missing_image = {
		attach: function (context, settings) {
			if ($('.region-above').height() == 0) {
				$('body').addClass('above-empty');
			}
		}
	};

	Drupal.behaviors.polaroid = {
		attach: function (context, settings) {
			$('img.image-style-polaroid').each(function () {
				var title = $(this).once().attr('title');
				if (title) {

					$(this).after('<div class="title">' + title + '</div>');
					$(this).parent().addClass('polaroid-image');
				}
			});
		}
	};

	Drupal.behaviors.experiences_carousel = {
		attach: function (context, settings) {
			$('.view-wat-mensen-over-ecy-dog-zeggen .view-content').once().slick({
				arrows: false,
				dots: true
			});
		}
	};

	Drupal.behaviors.mobile_menu = {
		attach: function (context, settings) {
			// Add the wrapper and the blinder to the main content.
			$('body').once().append('<div class="mobile-menu-blinder"></div><div class="mobile-menu-wrapper"></div>');

			// Find the main-menu and put it in the wrapper.
			let mainmenu = $('.menu--main > ul.menu').once().clone();
			mainmenu.addClass('main-menu');
			$('.mobile-menu-wrapper').append(mainmenu);

			// Second verse, same as the first, but now for the footer-menu.
			//	let footermenu = $('.menu--footer > ul.menu').once().clone();
			//	footermenu.addClass('footer-menu');
			//	$('.mobile-menu-wrapper').append(footermenu);

			// Add the menu-toggle behaviour
			$('.menu-toggler a').click(function () {
				$('body').addClass('mobile-menu-open');
			});

			// Add the closing behaviour.
			$('.mobile-menu-blinder').click(function () {
				$('body').removeClass('mobile-menu-open');
			});
		}
	};
})(jQuery);
